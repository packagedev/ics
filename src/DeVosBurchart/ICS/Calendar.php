<?php namespace DeVosBurchart\ICS;

use Closure;
use Exception;

/**
 * The Calendar class is the basis for the ics renderer.
 *
 * @author Roy De Vos Burchart <roy.devosburchart@comayers.com>
 * @since 1.0
 */

class Calendar {

	/**
	 * Calendar name (X-WR-CALNAME;VALUE=TEXT:)
	 *
	 * @var string
	 */
	protected $calname = null;

	/**
	 * Timezone (X-WR-TIMEZONE;VALUE=TEXT:)
	 *
	 * @var string
	 */
	protected $caltimezone = null;

	/**
	 * Version (VERSION)
	 *
	 * @var string
	 */
	protected $calversion = '2.0';

	/**
	 * Generator (PRODID)
	 *
	 * @var string
	 */
	protected $calgenerator = 'DeVosBurchart/Laravel';

	/**
	 * Events
	 *
	 * @var array
	 */
	protected $events = array();

	/**
	 * Timezone configurations
	 *
	 * @var array
	 */
	protected $timezones = array();

	/**
	 * Calendar Scale (CALSCALE)
	 *
	 * @var string
	 */
	protected $calscale = 'GREGORIAN';

	/**
	 * Set calendar name
	 *
	 * @param  string  $name
	 * @return \DeVosBurchart\ICS\Calendar
	 */
	public function name($value) {
		$this->calname = $value;
		return $this;
	}

	/**
	 * Set calendar scale
	 *
	 * @param  string  $name
	 * @return \DeVosBurchart\ICS\Calendar
	 */
	public function scale($value) {
		$this->calscale = $value;
		return $this;
	}

	/**
	 * Set timezone
	 *
	 * @param  \Closure  $callback
	 * @return \DeVosBurchart\ICS\Calendar
	 */
	public function timezone($type, $callback = null) {
		if(is_null($callback)) {
			$this->caltimezone = $type;
		} else {
			$timezone = new Timezone();

			if(!$callback instanceof Closure) throw new Exception('Argument 1 must be a closure.');

			call_user_func($callback, $timezone);

			$this->timezones[] = array('type' => $type, 'data' => $timezone);
		}
		
		return $this;
	}

	/**
	 * Create an event
	 *
	 * @param  \Closure  $callback
	 * @return \DeVosBurchart\ICS\Calendar
	 */
	public function event($callback, $timezone = null) {
		$timezone = (is_null($timezone)) ? $this->caltimezone : $timezone;

		$event = new Event($timezone);

		if(!$callback instanceof Closure) throw new Exception('Argument 1 must be a closure.');

		call_user_func($callback, $event);

		$this->events[] = $event;

		return $this;
	}

	/**
	 * Convert to ics format
	 *
	 * @return string
	 */
	public function __toString() {
		$data[] = 'BEGIN:VCALENDAR';
		$data[] = 'VERSION:' . $this->calversion;
		if(!is_null($this->calname)) $data[] = 'X-WR-CALNAME;VALUE=TEXT:' . $this->calname;
		if(!is_null($this->caltimezone)) $data[] = 'X-WR-TIMEZONE;VALUE=TEXT:' . $this->caltimezone;
		$data[] = 'PRODID:+//' . $this->calgenerator . '//NONSGML v1.0//EN';
		$data[] = 'CALSCALE:' . $this->calscale;

		if(count($this->timezones) > 0) {
			$data[] = 'BEGIN:VTIMEZONE';
			$data[] = 'TZID:' . $this->caltimezone;

			foreach($this->timezones as $timezone) {
				$data[] = 'BEGIN:' . strtoupper($timezone['type']);
				$data[] = (string) $timezone['data'];
				$data[] = 'END:' . strtoupper($timezone['type']);
			}

			$data[] = 'END:VTIMEZONE';
		}

		foreach($this->events as $event) {
			$data[] = (string) $event;
		}

		$data[] = 'END:VCALENDAR';

		return implode(PHP_EOL, $data);
	}

}