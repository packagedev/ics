<?php namespace DeVosBurchart\ICS;

use Exception;
use Closure;

/**
 * The Repeat class is called from the Event and Timezone class
 * to create the RRULE sections
 *
 * @author Roy De Vos Burchart <roy.devosburchart@comayers.com>
 * @since 1.0
 */

class Repeat {
	
	/**
	 * Supported frequencies
	 *
	 * @var string
	 */
	private $allowFreq = array('d' => 'daily','w' => 'weekly','m' => 'monthly','y' => 'yearly','h' => 'hourly','i' => 'minutely');

	/**
	 * Supported week days
	 *
	 * @var string
	 */
	private $allowWkst = array('mo','tu','we','th','fr','sa','su');

	/**
	 * Order of recurrence rules
	 *
	 * @var string
	 */
	private $recurrenceOrder = array(
		'MONTH',
		'WEEKNO',
		'YEARDAY',
		'MONTHDAY',
		'DAY',
		'HOUR',
		'MINUTE',
		'SECOND',
		'SETPOS'
	);

	/**
	 * Frequency
	 *
	 * @var string
	 */
	protected $r_freq = null;

	/**
	 * Frequency interval
	 *
	 * @var string
	 */
	protected $r_interval = null;

	/**
	 * Repeat count
	 *
	 * @var string
	 */
	protected $r_count = null;

	/**
	 * Until date
	 *
	 * @var string
	 */
	protected $r_until = null;

	/**
	 * Recurrences
	 *
	 * @var string
	 */
	protected $r_recur = array();

	/**
	 * Week start
	 *
	 * @var string
	 */
	protected $r_wkst = null;

	/**
	 * Constructor
	 *
	 * @param  string  $freq
	 * @param  integer  $interval
	 * @return void
	 */
	function __construct($freq = null, $interval = null) {
		if(!is_null($freq)) $this->freq($freq);
		if(!is_null($interval)) $this->interval($interval);
	}

	/**
	 * Set frequency
	 *
	 * @param  string  $freq
	 * @return \DeVosBurchart\ICS\Repeat
	 */
	function freq($freq) {
		if(!array_key_exists($freq,$this->allowFreq)) throw new Exception('Invalid frequency. Allowed options: d, w, m, y, h, i');

		$this->r_freq = $freq;

		return $this;
	}

	/**
	 * Set interval
	 *
	 * @param  string  $interval
	 * @return \DeVosBurchart\ICS\Repeat
	 */
	function interval($interval) {
		if(!is_numeric($interval)) throw new Exception('Interval has to be numeric.');

		$this->r_interval = $interval;

		return $this;
	}

	/**
	 * Set count
	 *
	 * @param  string  $count
	 * @return \DeVosBurchart\ICS\Repeat
	 */
	function count($amount) {
		if(!is_null($this->r_until)) throw new Exception('Until and Count cannot be set at the same time.');
		if(!is_numeric($amount)) throw new Exception('Amount has to be numeric.');

		$this->r_count = $amount;

		return $this;
	}

	/**
	 * Set until date
	 *
	 * @param  string  $date
	 * @param  boolean  $unix
	 * @return \DeVosBurchart\ICS\Repeat
	 */
	function until($date, $unix = false) {
		if(!is_null($this->r_count)) throw new Exception('Until and Count cannot be set at the same time.');
		if(!is_scalar($date)) throw new Exception('Until Date has to be scalar');

		$this->r_until = date('Ymd\THis', ($unix) ? $date : strtotime($date));

		return $this;
	}

	/**
	 * Set hours
	 *
	 * @return \DeVosBurchart\ICS\Repeat
	 */
	function hour() {
		$hours = func_get_args();
		$regex = '/^([01]?[0-9]|2[0-3])$/';
		foreach($hours as $arg => $hour) {
			if(preg_match($regex, $hour) === false) throw new Exception('Hour #' . ($arg+1) . ' is not valid');
		}

		$this->r_recur['HOUR'] = $hours;

		return $this;
	}

	/**
	 * Set minutes
	 *
	 * @return \DeVosBurchart\ICS\Repeat
	 */
	function minute() {
		$minutes = func_get_args();
		$regex = '/^([0-5]?[0-9])$/';
		foreach($minutes as $arg => $minute) {
			if(preg_match($regex, $minute) === false) throw new Exception('Minute #' . ($arg+1) . ' is not valid');
		}

		$this->r_recur['MINUTE'] = $minutes;

		return $this;
	}

	/**
	 * Set week days
	 *
	 * @return \DeVosBurchart\ICS\Repeat
	 */
	function day() {
		$days = func_get_args();
		$regex = '/^-?([0-9]+)$/';
		foreach($days as $arg => $day) {
			if(!in_array(substr(strtolower($day),-2), $this->allowWkst) || preg_match($regex, substr($day, 0, -2)) === false) throw new Exception('Day #' . ($arg+1) . ' is not valid');
		}

		$this->r_recur['DAY'] = $days;

		return $this;
	}

	/**
	 * Set months
	 *
	 * @return \DeVosBurchart\ICS\Repeat
	 */
	function month() {
		$months = func_get_args();
		$regex = '/^([1-9]|1[0-2])$/';
		foreach($months as $arg => $month) {
			if(preg_match($regex, $month) === false) throw new Exception('month #' . ($arg+1) . ' is not valid');
		}

		$this->r_recur['MONTH'] = $months;

		return $this;
	}

	/**
	 * Set days of month
	 *
	 * @return \DeVosBurchart\ICS\Repeat
	 */
	function monthDay() {
		$days = func_get_args();
		$regex = '/^-?([1-9]|[12][0-9]|3[0-1])$/';
		foreach($days as $arg => $day) {
			if(preg_match($regex, $day) === false) throw new Exception('Day #' . ($arg+1) . ' is not valid');
		}

		$this->r_recur['MONTHDAY'] = $days;

		return $this;
	}

	/**
	 * Set days of year
	 *
	 * @return \DeVosBurchart\ICS\Repeat
	 */
	function yearDay() {
		$days = func_get_args();
		$regex = '/^-?([1-9]|[1-9][0-9]|[12][0-9][0-9]|3[0-5][0-9]|36[0-6])$/';
		foreach($days as $arg => $day) {
			if(preg_match($regex, $day) === false) throw new Exception('Day #' . ($arg+1) . ' is not valid');
		}

		$this->r_recur['YEARDAY'] = $days;

		return $this;
	}

	/**
	 * Set weeks
	 *
	 * @return \DeVosBurchart\ICS\Repeat
	 */
	function week() {
		$weeks = func_get_args();
		$regex = '/^-?([1-9]|[1-4][0-9]|5[0-3])$/';
		foreach($days as $arg => $day) {
			if(preg_match($regex, $day) === false) throw new Exception('Day #' . ($arg+1) . ' is not valid');
		}

		$this->r_recur['WEEKNO'] = $weeks;

		return $this;
	}

	/**
	 * Set position
	 *
	 * @param  string  $freq
	 * @return \DeVosBurchart\ICS\Repeat
	 */
	function setPos($pos) {
		$regex = '/^-?([1-9]|[1-9][0-9]|[12][0-9][0-9]|3[0-5][0-9]|36[0-6])$/';
		if(preg_match($regex, $pos) === false) throw new Exception('Set Pos is not valid');

		$this->r_recur['SETPOS'] = $pos;

		return $this;
	}

	/**
	 * Set week start
	 *
	 * @param  string  $day
	 * @return \DeVosBurchart\ICS\Repeat
	 */
	function weekStart($day) {
		if(!in_array($day, $this->allowWkst)) throw new Exception('Week Start Day is not valid');

		$this->r_wkst = $day;

		return $this;
	}

	/**
	 * Convert to ics format
	 *
	 * @return string
	 */
	function __toString() {

		// Frequency
		$rrule[] = 'FREQ=' . strtoupper($this->allowFreq[$this->r_freq]);

		// Interval
		if(!is_null($this->r_interval)) $rrule[] = 'INTERVAL=' . $this->r_interval;

		// Recurrence
		if(count($this->r_recur) > 0) {
			$recurrenceOrder = array_flip($this->recurrenceOrder);
			uksort($this->r_recur, function($a, $b) use ($recurrenceOrder) {
				return ($recurrenceOrder[$a] > $recurrenceOrder[$b]) ? -1 : 1;
			});

			foreach($this->r_recur as $type => $recur) {
				$rrule[] = 'BY' . strtoupper($type) . '=' . strtoupper(implode(',',$recur));
			}
		}

		// Endings
		if(!is_null($this->r_count)) $rrule[] = 'COUNT=' . $this->r_count;
		if(!is_null($this->r_until)) $rrule[] = 'UNTIL=' . $this->r_until;

		return implode(';',$rrule);
	}

}