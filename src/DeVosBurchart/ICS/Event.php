<?php namespace DeVosBurchart\ICS;

use Exception;
use Closure;

/**
 * The Event class is called from the Calendar class
 * to create the event sections of the ics file
 *
 * @author Roy De Vos Burchart <roy.devosburchart@comayers.com>
 * @since 1.0
 */

class Event {
	
	/**
	 * Unique ID
	 *
	 * @var string
	 */
	protected $event_uid = null;

	/**
	 * Created at
	 *
	 * @var array
	 */
	protected $event_created_at = null;

	/**
	 * Location
	 *
	 * @var string
	 */
	protected $event_location = null;

	/**
	 * Location
	 *
	 * @var string
	 */
	protected $event_timezone = null;

	/**
	 * Name
	 *
	 * @var string
	 */
	protected $event_name = null;

	/**
	 * Description
	 *
	 * @var string
	 */
	protected $event_description = null;

	/**
	 * Start date
	 *
	 * @var string
	 */
	protected $event_start = null;

	/**
	 * End date
	 *
	 * @var string
	 */
	protected $event_end = null;

	/**
	 * Duration
	 *
	 * @var string
	 */
	protected $event_duration = null;

	/**
	 * Repeat
	 *
	 * @var DeVosBurchart\ICS\Repeat
	 */
	protected $event_repeat = null;

	/**
	 * Constructor
	 *
	 * @param  string  $timezone
	 * @return void
	 */
	public function __construct($timezone) {
		$this->event_timezone = ($timezone) ? $timezone : null;
	}

	/**
	 * Set the Created at date
	 *
	 * @param  string  $date
	 * @param  boolean  $unix
	 * @return \DeVosBurchart\ICS\Event
	 */
	public function created_at($date, $unix = false) {
		if(!is_scalar($date)) throw new Exception('Created at has to be scalar value');

		$this->event_created_at = date('Ymd\THis', ($unix) ? $date : strtotime($date));
		return $this;
	}

	/**
	 * Set the Start Date
	 *
	 * @param  string  $date
	 * @param  boolean  $unix
	 * @return \DeVosBurchart\ICS\Event
	 */
	public function start($date, $unix = false) {
		if(!is_scalar($date)) throw new Exception('Start date has to be scalar value');

		$this->event_start = $this->dateFormatter( ($unix) ? $date : strtotime($date));
		return $this;
	}

	/**
	 * Set the End Date
	 *
	 * @param  string  $date
	 * @param  boolean  $unix
	 * @return \DeVosBurchart\ICS\Event
	 */
	public function end($date, $unix = false) {
		if(!is_scalar($date)) throw new Exception('End date has to be scalar value');

		$this->event_end = $this->dateFormatter(($unix) ? $date : strtotime($date));
		return $this;
	}

	/**
	 * Set the Name
	 *
	 * @param  string  $name
	 * @return \DeVosBurchart\ICS\Event
	 */
	public function name($name) {
		if(!is_scalar($name)) throw new Exception('Name has to be scalar value');

		$this->event_name = $name;
		return $this;
	}

	/**
	 * Set the Location
	 *
	 * @param  string  $location
	 * @return \DeVosBurchart\ICS\Event
	 */
	public function location($location) {
		if(!is_scalar($location)) throw new Exception('Location has to be scalar value');

		$this->event_location = $location;
		return $this;
	}

	/**
	 * Set the Description
	 *
	 * @param  string  $desc
	 * @return \DeVosBurchart\ICS\Event
	 */
	public function description($desc) {
		if(!is_scalar($desc)) throw new Exception('Description has to be scalar value');

		$this->event_description = str_replace("\n","\\n",$desc);
		return $this;
	}

	/**
	 * Set the Duration
	 *
	 * @param  int  $hour
	 * @param  int  $minute
	 * @param  int  $day
	 * @return \DeVosBurchart\ICS\Event
	 */
	public function duration($hour = 0, $minute = 0, $day = 0) {
		if(!is_scalar($hour) || !is_scalar($minute) || !is_scalar($day)) throw new Exception('Duration has to be scalar value');

		$this->event_duration = 'PT' . $day . 'D' . $hour . 'H' . $minute . 'M';
		return $this;
	}

	/**
	 * Set a repeater
	 *
	 * @param  Closure|string  $callback
	 * @param  integer  $interval
	 * @return \DeVosBurchart\ICS\Event
	 */
	public function repeat($callback, $interval = null) {
		if(!$callback instanceof Closure && !is_scalar($callback)) throw new Exception('Argument 1 must be a closure or a scalar (frequency).');
		if(!is_null($interval) && !is_numeric($interval)) throw new Exception('Argument 2 must be a closure or a scalar (frequency).');

		if($callback instanceof Closure) {
			$repeat = new Repeat();
			call_user_func($callback, $repeat);
		} else {
			$repeat = new Repeat($callback, $interval);
		}

		$this->event_repeat = $repeat;

		return $this;
	}

	/**
	 * Convert to ics format
	 *
	 * @return string
	 */
	public function __toString() {
		$timezone = (!is_null($this->event_timezone)) ? ';TZID=' . $this->event_timezone : '';

		$data[] = 'BEGIN:VEVENT';
		$data[] = 'UID:' . (is_null($this->event_uid) ? uniqid() : $this->event_uid);
		$data[] = 'DTSTAMP' . $timezone . ':' . (is_null($this->event_created_at) ? $this->dateFormatter(time()) : $this->event_created_at);
		$data[] = 'DTSTART' . $timezone . ':' . $this->event_start;
		if(!is_null($this->event_end)) $data[] = 'DTEND' . $timezone . ':' . $this->event_end;
		if(!is_null($this->event_duration)) $data[] = 'DURATION:' . $this->event_duration;
		if(!is_null($this->event_location)) $data[] = 'LOCATION:' . $this->event_location;
		if(!is_null($this->event_repeat)) $data[] = 'RRULE:' . $this->event_repeat;
		$data[] = 'SUMMARY:' . $this->event_name;
		$data[] = 'DESCRIPTION:' . $this->event_description;
		$data[] = 'END:VEVENT';

		return implode(PHP_EOL, $data);
	}

	private function dateFormatter($timestamp) {
		return date('Ymd\THis', $timestamp);
	}
	
}