<?php namespace DeVosBurchart\ICS;

use Exception;
use Closure;

/**
 * The Timezone class is called from the Calendar class to set
 * the global timezone settings
 *
 * @author Roy De Vos Burchart <roy.devosburchart@comayers.com>
 * @since 1.0
 */

class Timezone {

	/**
	 * Start date
	 *
	 * @var string
	 */
	protected $t_start = null;

	/**
	 * Repeat rules
	 *
	 * @var \DeVosBurchart\ICS\Repeat
	 */
	protected $t_repeat = null;

	/**
	 * Timezone name
	 *
	 * @var string
	 */
	protected $t_name = null;

	/**
	 * From Offset
	 *
	 * @var string
	 */
	protected $t_from = null;

	/**
	 * To Offset
	 *
	 * @var string
	 */
	protected $t_to = null;

	/**
	 * Create an event
	 *
	 * @param  string  $date
	 * @param  boolean  $unix
	 * @return \DeVosBurchart\ICS\Timezone
	 */
	function start($date, $unix = false) {
		if(!is_scalar($date)) throw new Exception('Start date has to be scalar value');

		$this->t_start = date('Ymd\THis', ($unix) ? $date : strtotime($date));

		return $this;
	}

	/**
	 * Set timezone repeat
	 *
	 * @param  \Closure  $callback
	 * @return \DeVosBurchart\ICS\Timezone
	 */
	function repeat($callback) {
		$repeat = new Repeat();

		if(!$callback instanceof Closure) throw new Exception('Argument 1 must be a closure.');

		call_user_func($callback, $repeat);

		$this->t_repeat = $repeat;

		return $this;
	}

	/**
	 * Set Timezone name
	 *
	 * @param  string  $name
	 * @return \DeVosBurchart\ICS\Timezone
	 */
	function name($name) {
		if(!is_scalar($name)) throw new Exception('Name has to be scalar value');

		$this->t_name = $name;

		return $this;
	}

	/**
	 * Set From offset
	 *
	 * @param  string  $offset
	 * @return \DeVosBurchart\ICS\Timezone
	 */
	function from($offset) {
		if(!is_scalar($offset)) throw new Exception('Offset From has to be scalar value');

		$this->t_from = $offset;

		return $this;
	}

	/**
	 * Set To offset
	 *
	 * @param  string  $offset
	 * @return \DeVosBurchart\ICS\Timezone
	 */
	function to($offset) {
		if(!is_scalar($offset)) throw new Exception('Offset To has to be scalar value');

		$this->t_to = $offset;

		return $this;
	}

	/**
	 * Convert to ics format
	 *
	 * @return string
	 */
	function __toString() {
		if(!is_null($this->t_start)) $tz[] = 'DTSTART:' . $this->t_start;
		if(!is_null($this->t_repeat)) $tz[] = 'RRULE:' . (string) $this->t_repeat;
		if(!is_null($this->t_from)) $tz[] = 'TZOFFSETFROM:' . $this->t_from;
		if(!is_null($this->t_to)) $tz[] = 'TZOFFSETTO:' . $this->t_to;
		if(!is_null($this->t_name)) $tz[] = 'TZNAME:' . $this->t_name;

		return implode(PHP_EOL, $tz);
	}

}